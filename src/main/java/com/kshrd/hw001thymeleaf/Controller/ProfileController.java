package com.kshrd.hw001thymeleaf.Controller;

import com.kshrd.hw001thymeleaf.Model.ProfileInfo;
import com.kshrd.hw001thymeleaf.Service.ProfileInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping("/Profile")
public class ProfileController {
    private final ProfileInfoService profileInfoService;
    @Autowired
    public ProfileController(ProfileInfoService profileInfoService) {
        this.profileInfoService = profileInfoService;
    }

    @GetMapping("")
    public String showAllProfileInfo(ModelMap modelMap){
        modelMap.addAttribute("profileInfoList",profileInfoService.findAll());
        return "index";
    }

    @GetMapping("/add")
    public String showAddProfileinfo(@ModelAttribute ProfileInfo profileInfo, ModelMap modelMap){
        modelMap.addAttribute(profileInfo);
        return "form-add";
    }

    @PostMapping("/add")
    public String addProfileInfo(@ModelAttribute  ProfileInfo profileInfo){
        List<ProfileInfo> profileInfoList = profileInfoService.findAll();
        profileInfo.setId(profileInfoList.size()+1);
        profileInfoService.findAll().add(profileInfo);
        return "redirect:/Profile";
    }
}
