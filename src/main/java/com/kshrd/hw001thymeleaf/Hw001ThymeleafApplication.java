package com.kshrd.hw001thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hw001ThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(Hw001ThymeleafApplication.class, args);
    }

}
