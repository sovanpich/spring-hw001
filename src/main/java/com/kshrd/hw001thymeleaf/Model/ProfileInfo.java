package com.kshrd.hw001thymeleaf.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProfileInfo {
    private int id;
    private String name;
    private String description;

}
