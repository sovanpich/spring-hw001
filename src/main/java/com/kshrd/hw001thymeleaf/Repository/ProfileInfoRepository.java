package com.kshrd.hw001thymeleaf.Repository;

import com.kshrd.hw001thymeleaf.Model.ProfileInfo;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ProfileInfoRepository {
    List<ProfileInfo> profileInfoList = new ArrayList<>();
    {
        profileInfoList.add(new ProfileInfo(1,"Pich","pp"));
        profileInfoList.add(new ProfileInfo(2,"Dra","sv"));
    }

    public List<ProfileInfo> findAll(){
        return profileInfoList;
    }

    public void addNew(ProfileInfo profileInfo){
        profileInfoList.add(profileInfo);
    }
}
