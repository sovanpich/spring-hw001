package com.kshrd.hw001thymeleaf.Service.Imp;

import com.kshrd.hw001thymeleaf.Model.ProfileInfo;
import com.kshrd.hw001thymeleaf.Repository.ProfileInfoRepository;
import com.kshrd.hw001thymeleaf.Service.ProfileInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfileInfoServiceImp implements ProfileInfoService {

    private final ProfileInfoRepository profileInfoRepository;
    @Autowired
    public ProfileInfoServiceImp(ProfileInfoRepository profileInfoRepository) {
        this.profileInfoRepository=profileInfoRepository;
    }

    @Override
    public List<ProfileInfo> findAll() {
        return profileInfoRepository.findAll();
    }

    @Override
    public void addNew (ProfileInfo profileInfo) { profileInfoRepository.addNew(profileInfo); }
}
