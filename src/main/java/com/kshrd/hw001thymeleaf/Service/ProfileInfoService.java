package com.kshrd.hw001thymeleaf.Service;

import com.kshrd.hw001thymeleaf.Model.ProfileInfo;

import java.util.List;

public interface ProfileInfoService {
    public List<ProfileInfo> findAll();
    public void addNew(ProfileInfo profileInfo);
}
